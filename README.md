# PXE BOOT

Er moet een PXE Boot server zijn, waarop ook de cloud-init bestanden worden gehost

# LINKS

https://gitlab.com/caner.yilmaz/cloud-init-kloten
https://eradman.com/posts/automated-ubuntu-install.html
https://cloudinit.readthedocs.io/en/23.3.3/tutorial/qemu.html

# SQUID PROXY CACHE

https://eradman.com/posts/mirror-cache.html
Cache maken voor veel geserveerde packages. bijvoorbeeld Apt

# NOTES

# FTP SERVER (IMDS SERVICE)

`python3 -m http.server --directory .`

# REPOSITORY

https://gitlab.com/caner.yilmaz/cloud-init-kloten
run ./prepare to download iso image

# ISO IMAGE

download latest image: `wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img`
verify:

# PXE Server

staat op 10.0.1.1:69, wordt docker-tftp gebruikt

TFTP-HA gebruikt
Syslinux gedownload en dit gecopieerd

```
cp efi64/com32/elflink/ldlinux/ldlinux.e64 UEFI
cp efi64/com32/libutil/libutil.c32 UEFI
cp efi64/com32/menu/menu.c32 UEFI
cp efi64/com32/menu/vesamenu.c32 UEFI
cp efi64/efi/syslinux.efi UEFI
```

```
cp bios/com32/menu/menu.c32 /tftpboot/BIOS
cp bios/core/pxelinux.0 /tftpboot/BIOS
cp bios/com32/elflink/ldlinux/ldlinux.c32 /tftpboot/BIOS
cp bios/com32/libutil/libutil.c32 /tftpboot/BIOS
cp bios/com32/menu/menu.c32 /tftpboot/BIOS
cp bios/com32/menu/vesamenu.c32 /tftpboot/BIOS
cp bios/memdisk/memdisk /tftpboot/kernels
```
