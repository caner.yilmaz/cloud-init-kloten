#!/bin/bash
set -o errexit
set -u pipefail
set -o nounset

REPO_FOLDER="$(dirname "$0")"
JAMMY_SERVER_FOLDER=$REPO_FOLDER/iso/jammy-server
JAMMY_IMG=jammy-server-cloudimg-amd64.img

prepare_iso() {
	mkdir -p "$REPO_FOLDER"/iso
	mkdir -p "$JAMMY_SERVER_FOLDER"

	download_iso
	#	checksum_iso

}
download_iso() {
	if [ -f "$JAMMY_SERVER_FOLDER"/"$JAMMY_IMG" ]; then
		echo "[INFO] cloud image found"
	else
		echo "[INFO] cloud image doesn't exist"
		wget https://cloud-images.ubuntu.com/jammy/current/"$JAMMY_IMG" -P "$JAMMY_SERVER_FOLDER"
	fi
}
checksum_iso() {
	echo "to be implemented"
}

main() {
	echo "[INFO] Started preparing repistory"
	prepare_iso
}

main
