#!/bin/bash
#
# Unattended Qt Framework installation script for GENIUS development environment.
#
# For reference see: https://wiki.qt.io/Online_Installer_4.x
#                    https://www.qt.io/blog/qt-installer-framework-4.0-released
#
# To find packages used the search option: https://wiki.qt.io/Online_Installer_4.x#Search
#
set -e #exit on error

INPUT_VALID=0

# Get input arguments
while getopts u:p: option; do
	case "${option}" in
	u)
		USER_EMAIL=${OPTARG}
		let "INPUT_VALID = $INPUT_VALID + 1"
		;;
	p)
		USER_PASSWORD=${OPTARG}
		let "INPUT_VALID = $INPUT_VALID + 1"
		;;
	\?)
		echo "Invalid option"
		echo "-u : User E-Mail"
		echo "-p : User Password"
		exit 1
		;;
	:)
		echo "Invalid option: -$OPTARG requires an argument"
		exit 1
		;;
	esac
done

# Check both options were given
if [ ! $INPUT_VALID = 2 ]; then

	echo "[ERROR] Both User email & password need to be provided!"
	exit 1
fi

# Make temp dir for Qt installer
mkdir -p /home/$USER/tmp/qt/

# Remove old installer if it exists
if [ -f "/tmp/qt/installer.run" ]; then
	rm /home/$USER/tmp/qt/installer.run
fi

INSTALL_DIR=/home/$USER/Qt/

# Download latest Qt Online Installer
curl -Lo /home/$USER/tmp/qt/installer.run "https://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run"

# Add execute permission
chmod u+x /home/$USER/tmp/qt/installer.run

# qt.tools.openssl
# qt.tools.openssl.src
# qt.tools.openssl.gcc_64
# qt.qt5.51210
# qt.qt5.51210.gcc_64
# qt.qt5.51210.debug_info

INSTALLER_PACKAGES=""
INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.tools.openssl.src"
INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.tools.openssl.gcc_64"
INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.qt5.5152.gcc_64"
INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.qt5.5152.debug_info"

# Qt 6 packages -- temp disabled till we figure out which LTS we use
# INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.qt6.624.gcc_64"
# INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.qt6.624.addons.qtwebsockets"
# INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.qt6.624.addons.qtpositioning"
# INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.qt6.624.addons.qtserialport"
# INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.qt6.624.addons.qtmultimedia"
# INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.qt6.624.qt5compat"
# INSTALLER_PACKAGES="$INSTALLER_PACKAGES qt.qt6.624.debug_info"

INSTALLER_OPTIONS=""
INSTALLER_OPTIONS="$INSTALLER_OPTIONS --root $INSTALL_DIR"
INSTALLER_OPTIONS="$INSTALLER_OPTIONS --auto-answer telemetry-question=No,AssociateCommonFiletypes=Yes"
INSTALLER_OPTIONS="$INSTALLER_OPTIONS --default-answer"
INSTALLER_OPTIONS="$INSTALLER_OPTIONS --accept-licenses --accept-obligations"
INSTALLER_OPTIONS="$INSTALLER_OPTIONS --confirm-command"

INSTALLER_COMMAND="install"

INSTALLER_CREDENTIALS="--email $USER_EMAIL --password $USER_PASSWORD"

#
/home/$USER/tmp/qt/installer.run $INSTALLER_OPTIONS $INSTALLER_CREDENTIALS $INSTALLER_COMMAND $INSTALLER_PACKAGES
