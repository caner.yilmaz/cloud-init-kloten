## Provision machine

1. Install ansible:

```bash
pip3 install ansible
ansible-galaxy install arillso.ntp
ansible-galaxy collection install community.general
```

2. Maak een VM in bridged network mode
3. installeer ubuntu 2204 desktop.

4. move .ansible.cfg to ~/.ansible.cfg

5. edit `inventory` en add ip as:
   NAME-VM ansible-host=10.123.1.1

6. run: `ansible-playbook bootstrap.yml -i inventory -v`
