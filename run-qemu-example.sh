qemu-system-x86_64 \
	-net nic \
	-net user \
	-machine accel=kvm:tcg \
	-cpu host \
	-m 5120 \
	-nographic \
	-hda ./iso/jammy-server/jammy-server-cloudimg-amd64.img \
	-smbios type=1,serial=ds='nocloud;s=http://10.0.10.100:8000/config/'
