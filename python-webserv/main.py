from fastapi import FastAPI, Form
from typing import Annotated

app = FastAPI()

@app.get("/")
async def root():
    return {"Heyya!!"}

@app.post("/phone_home")
def read_instance(pub_key_rsa: Annotated[str, Form()], pub_key_ed25519: Annotated[str, Form()], pub_key_ecdsa: Annotated[str, Form()], instance_id: Annotated[str, Form()], hostname: Annotated[str, Form()], fqdn: Annotated[str, Form()]):
    with open("test.txt", "a") as myfile:
      myfile.write("pub_key_ecdsa: " + pub_key_ecdsa + "\n")
      myfile.write("pub_key_ed25519: " + pub_key_ed25519 + "\n")
      myfile.write("pub_key_rsa: " +pub_key_rsa + "\n")
      myfile.write("instance_id: " + instance_id + "\n")
      myfile.write("hostname: " + hostname + "\n")
      myfile.write("fqdn: " + fqdn + "\n")
      myfile.close()

    return {"ok": "ok"}
